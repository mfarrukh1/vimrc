set nocompatible              " required
filetype off                  " required

set encoding=utf-8
set clipboard=unnamedplus
set splitbelow
set splitright
set nu rnu
set hls is
set showcmd
" Switch between split

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
" Tab spacing and indntation
set tabstop=4
set shiftwidth=4
set expandtab

" Enable folding
set foldmethod=indent
set foldlevel=99

" Enable folding with the spacebar
nnoremap <space> za

" Execute current file
nnoremap <F9> :!%:p<Enter>

" Flag unnecessary whitespace
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match Cursor /\s\+$/

" Cursor indcation in INSERT mode
au InsertEnter * set cursorline
au InsertLeave * set nocursorline

py << EOF
import os
import sys
if 'VIRTUAL_ENV' in os.environ:
    project_base_dir = os.environ['VIRTUAL_ENV']
    activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
    execfile(activate_this, dict(__file__=activate_this))
EOF


" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" add all your plugins here (note older versions of Vundle
" used Bundle instead of Plugin)

" ...

Plugin 'vim-syntastic/syntastic'

Plugin 'scrooloose/nerdtree'

Plugin 'jistr/vim-nerdtree-tabs'

Plugin 'tpope/vim-fugitive'

Plugin 'valloric/youcompleteme'

Plugin 'vim-airline/vim-airline'


Plugin 'nvie/vim-flake8'
" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

let g:SimpylFold_docstring_preview=1
let g:loaded_youcompleteme=1
let g:ycm_autoclose_preview_window_after_completion=1
let python_highlight_all=1
syntax on

map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>
if has('gui_running')
    set background=dark
    colorscheme solarized
else
    colorscheme distinguished 
endif

let NERDTreeIgnore=['\.pyc$', '\~$'] "ignore files in NERDTree
let NERDTreeShowHidden=1

" Start NERDTree
"autocmd vimenter * NERDTree
" Airline symbols
let g:airline_powerline_fonts=1
